
XMLittré
========

Avertissement
-------------

Le XML du Littré est imparfaitement balisé. Certains éléments ne sont
pas balisés quand ils devraient l'être. Plus grave, certaines balises
sont fautives. Tout ceci est le résultat de scripts mais aussi de très
nombreuses corrections manuelles.


Données
-------

Un fichier XML en UTF-8 pour chaque lettre de l'alphabet.
Une DTD (approximative) est fournie.


Fidélité du texte
------------------

* Les caractères non latins, et donc les mots grecs, ne sont pas présents.

* Il est possible que le balisage ait entraîné des dégâts involontaires.

* La ponctuation originale est conservée autant que possible,
  mais ce n'est pas une priorité.

* Dans certains cas, le texte a été corrigé.
  Par exemple, dans ATOURNER :

    Moult m'a amors atornée Douce peine et biau labor, *Couci*, I.
    En perilleuse aventure M'avez, amours, atorné, ib. IV.

    Moult m'a amors atornée Douce peine et biau labor, *Couci*, I.
    En perilleuse aventure M'avez, amours, atorné, ID. ib. IV.

  *Couci* est un nom d'œuvre. D'ailleurs dans le texte original,
  il n'est pas écrit en majuscules. Mais comme ce sont des mémoires,
  supposément écrites par le sire de Couci, Littré le prend parfois
  comme un nom d'auteur.

  La confusion ID/ib est assez fréquente, pas seulement avec Couci.
  L'attribut `fix="ID"` a été ajouté dans ce cas.


Conditions d'utilisation
------------------------

Les données balisées sont toutes placées sous licence *Creative Commons
Paternité - Partage des conditions initiales à l'identique 3.0*,
alias **CC-BY-SA 3.0**. En particulier, vous ne pouvez donc les utiliser
qu'en indiquent leur source « http://littre.org » ou en faisant
référence à l'auteur du XML « François Gannaz, francois.gannaz@littre.org ».


Formes fléchies et lexémisation
--------------------------------

C'est une demande récurrente. Si vous cherchez une liste de
mots déclinés sous toutes leurs formes (par exemple pour pouvoir
remonter de "fîmes" à "faire"), je vous recommande
[Lefff](http://atoll.inria.fr/~sagot/lefff.html).

Une autre solution d'excellente qualité est
[Morphalou](http://www.cnrtl.fr/lexiques/morphalou/), avec une licence
plus restrictive, mais avec la caution du [TlF](http://www.cnrtl.fr/definition/).



Contact
-------

Des informations éventuellement complémentaires sont dans la
[FAQ du site](http://littre.org/faq).

François Gannaz <francois.gannaz@littre.org>
